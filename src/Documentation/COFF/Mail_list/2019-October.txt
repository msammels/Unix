From jacob.ritorto at gmail.com  Fri Oct 11 02:52:34 2019
From: jacob.ritorto at gmail.com (Jacob Ritorto)
Date: Thu, 10 Oct 2019 12:52:34 -0400
Subject: [COFF] subscribe
Message-ID: <CAHYQbfDiHCiAKRyzknSJd-6B8mPgZ8a2MOaySREKWR=HtysOtg@mail.gmail.com>


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://minnie.tuhs.org/pipermail/coff/attachments/20191010/8857932c/attachment.html>

From michael at kjorling.se  Mon Oct 14 06:05:16 2019
From: michael at kjorling.se (Michael =?utf-8?B?S2rDtnJsaW5n?=)
Date: Sun, 13 Oct 2019 20:05:16 +0000
Subject: [COFF] Linux & UNIX Humble Bundle of ebooks
Message-ID: <z4f3mtzq4xdrffxw33mbx4zs@localhost>

Here's something I think some people here just might be interested in.

Humble Bundle is offering (for another just over a week; until
2019-10-21 18:00 -11:00) a "Linux & UNIX" ebook bundle from O'Reilly,
consisting of:

(at USD 1.00 or more)

- Classic Shell Scripting, 1st Edition
- Linux Device Drivers, 3rd Edition
- Introducing Regular Expressions, 1st Edition
- grep Pocket Reference, 1st Edition
- Learning GNU Emacs, 3rd Edition
- Unix Power Tools, 3rd Edition

(at USD 8.00 or more, all the above _plus_)

- Learning the bash Shell, 3rd Edition
- Learning the vi and Vim Editors, 7th Edition
- Linux in a Nutshell, 6th Edition
- sed & awk, 2nd Edition

(at USD 15.00 or more, all the above _plus_)

- bash Cookbook, 2nd Edition
- Linux System Programming, 2nd Edition
- Mastering Regular Expressions, 3rd Edition
- Effective awk Programming, 4th Edition
- Linux Pocket Guide, 3rd Edition

All in .mobi, .pdf and .epub formats, free of DRM.

Some of the proceeds from this offering go to Code for America.

No affiliation; just a happy customer.

https://www.humblebundle.com/books/linux-unix-oreilly-books

-- 
Michael Kjörling • https://michael.kjorling.se • michael at kjorling.se
  “The most dangerous thought that you can have as a creative person
              is to think you know what you’re doing.” (Bret Victor)


From peter at rulingia.com  Tue Oct 22 20:29:52 2019
From: peter at rulingia.com (Peter Jeremy)
Date: Tue, 22 Oct 2019 21:29:52 +1100
Subject: [COFF] [TUHS] Space Travel, was New: The Earliest UNIX Code
In-Reply-To: <CANOZ5rjGQeA3BFaMFXV3s-+11Ev9th2zerhvmBZetT6q0Eh2VQ@mail.gmail.com>
References: <201910191440.x9JEe8PB035921@tahoe.cs.Dartmouth.EDU>
 <CANOZ5rjGQeA3BFaMFXV3s-+11Ev9th2zerhvmBZetT6q0Eh2VQ@mail.gmail.com>
Message-ID: <20191022102952.GC51849@server.rulingia.com>

[Redirecting to COFF]

On 2019-Oct-20 00:02:56 +0530, Abhinav Rajagopalan <abhinavrajagopalan at gmail.com> wrote:
>Forgive me for both hijacking this thread, and to address my amateurish
>gnawing concern, but how was it be possible to write differential/integral
>equations at an assembly/machine level at the time, especially in machines
>such as the PDP-7 and such which had IIRC just 16 instructions and operated
>on the basis of mere words, especially the floating point math being done.

My 1st edition Wilkes, Wheeler, Gill[1] documents that, by 1951, EDSAC[2]
had a floating-point library that supported addition, subtraction and
multiplication (no division) of numbers with 23-27 bits of precision and a
range of 1e-63 to 1e63.  EDSAC was much less powerful than a PDP-7.

Writing a floating-point library is not that difficult, though getting
the rounding correct for all the edge cases is tricky.  Actually using
floating-point and avoiding the pitfalls can be harder - see (eg)
https://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html (though
https://floating-point-gui.de/ may be more approachable).

[1] https://archive.org/details/programsforelect00wilk
[2] https://en.wikipedia.org/wiki/EDSAC
-- 
Peter Jeremy
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 963 bytes
Desc: not available
URL: <http://minnie.tuhs.org/pipermail/coff/attachments/20191022/3245a005/attachment.sig>

From dave at horsfall.org  Tue Oct 29 04:47:33 2019
From: dave at horsfall.org (Dave Horsfall)
Date: Tue, 29 Oct 2019 05:47:33 +1100 (EST)
Subject: [COFF] [TUHS] UNIX Backslash History
In-Reply-To: <alpine.BSF.2.02.1910281106290.77349@frieza.hoshinet.org>
References: <201910272031.x9RKVSem124842@tahoe.cs.Dartmouth.EDU>
 <CAFH29trMHepMHK0C+UapNVXvjfnFMv5ov4W4YS+yOn4i+mhV0A@mail.gmail.com>
 <CANV78LQa=VTZAMmVeRphTvrkxrkrAzgoU_-KqtdgqfWY2uUZJg@mail.gmail.com>
 <51f2d838-d097-a93f-b44d-9c670d206d2b@tnetconsulting.net>
 <alpine.BSF.2.02.1910272110350.28402@frieza.hoshinet.org>
 <xzdtvssnnqfvfxq9swgv979g@localhost>
 <alpine.BSF.2.02.1910281106290.77349@frieza.hoshinet.org>
Message-ID: <alpine.BSF.2.21.9999.1910290544171.17400@aneurin.horsfall.org>

On Mon, 28 Oct 2019, Steve Nickolas wrote:

> 86-DOS actually did use ":" as a prompt character.  This was changed for 
> IBM's release, for some clone releases, and for MS-DOS 2.0.

The best I've ever seen was RT-11's "." - talk about minimalist...

Actually this thread probably belongs on COFF by now.

-- Dave

From lars at nocrew.org  Tue Oct 29 05:10:30 2019
From: lars at nocrew.org (Lars Brinkhoff)
Date: Mon, 28 Oct 2019 19:10:30 +0000
Subject: [COFF] Comparative promptology
In-Reply-To: <alpine.BSF.2.21.9999.1910290544171.17400@aneurin.horsfall.org>
 (Dave Horsfall's message of "Tue, 29 Oct 2019 05:47:33 +1100 (EST)")
References: <201910272031.x9RKVSem124842@tahoe.cs.Dartmouth.EDU>
 <CAFH29trMHepMHK0C+UapNVXvjfnFMv5ov4W4YS+yOn4i+mhV0A@mail.gmail.com>
 <CANV78LQa=VTZAMmVeRphTvrkxrkrAzgoU_-KqtdgqfWY2uUZJg@mail.gmail.com>
 <51f2d838-d097-a93f-b44d-9c670d206d2b@tnetconsulting.net>
 <alpine.BSF.2.02.1910272110350.28402@frieza.hoshinet.org>
 <xzdtvssnnqfvfxq9swgv979g@localhost>
 <alpine.BSF.2.02.1910281106290.77349@frieza.hoshinet.org>
 <alpine.BSF.2.21.9999.1910290544171.17400@aneurin.horsfall.org>
Message-ID: <7wsgnc4rfd.fsf_-_@junk.nocrew.org>

Dave Horsfall wrote:
> Steve Nickolas wrote:
>> 86-DOS actually did use ":" as a prompt character.
> The best I've ever seen was RT-11's "." - talk about minimalist...
>
> Actually this thread probably belongs on COFF by now.

I was bound to happen.  List all the prompts!

"*" seems popular on PDP-10s.

From imp at bsdimp.com  Tue Oct 29 05:57:01 2019
From: imp at bsdimp.com (Warner Losh)
Date: Mon, 28 Oct 2019 13:57:01 -0600
Subject: [COFF] Comparative promptology
In-Reply-To: <7wsgnc4rfd.fsf_-_@junk.nocrew.org>
References: <201910272031.x9RKVSem124842@tahoe.cs.Dartmouth.EDU>
 <CAFH29trMHepMHK0C+UapNVXvjfnFMv5ov4W4YS+yOn4i+mhV0A@mail.gmail.com>
 <CANV78LQa=VTZAMmVeRphTvrkxrkrAzgoU_-KqtdgqfWY2uUZJg@mail.gmail.com>
 <51f2d838-d097-a93f-b44d-9c670d206d2b@tnetconsulting.net>
 <alpine.BSF.2.02.1910272110350.28402@frieza.hoshinet.org>
 <xzdtvssnnqfvfxq9swgv979g@localhost>
 <alpine.BSF.2.02.1910281106290.77349@frieza.hoshinet.org>
 <alpine.BSF.2.21.9999.1910290544171.17400@aneurin.horsfall.org>
 <7wsgnc4rfd.fsf_-_@junk.nocrew.org>
Message-ID: <CANCZdfqQ3gq=Wtee7XOG9yhuWf7bQEhCsw1xOEvE+JgByadHtw@mail.gmail.com>

On Mon, Oct 28, 2019 at 1:51 PM Lars Brinkhoff <lars at nocrew.org> wrote:

> Dave Horsfall wrote:
> > Steve Nickolas wrote:
> >> 86-DOS actually did use ":" as a prompt character.
> > The best I've ever seen was RT-11's "." - talk about minimalist...
> >
> > Actually this thread probably belongs on COFF by now.
>
> I was bound to happen.  List all the prompts!
>
> "*" seems popular on PDP-10s.
>

"@  " was the TOPS-20 prompt.
"$ " was the VMS prompt
RSTS/E was just "Ready\n"

But none of these get us closer to CP/M's > prompt.

Warner
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://minnie.tuhs.org/pipermail/coff/attachments/20191028/1caced33/attachment.html>

From jpl.jpl at gmail.com  Tue Oct 29 06:29:28 2019
From: jpl.jpl at gmail.com (John P. Linderman)
Date: Mon, 28 Oct 2019 16:29:28 -0400
Subject: [COFF] Comparative promptology
In-Reply-To: <CANCZdfqQ3gq=Wtee7XOG9yhuWf7bQEhCsw1xOEvE+JgByadHtw@mail.gmail.com>
References: <201910272031.x9RKVSem124842@tahoe.cs.Dartmouth.EDU>
 <CAFH29trMHepMHK0C+UapNVXvjfnFMv5ov4W4YS+yOn4i+mhV0A@mail.gmail.com>
 <CANV78LQa=VTZAMmVeRphTvrkxrkrAzgoU_-KqtdgqfWY2uUZJg@mail.gmail.com>
 <51f2d838-d097-a93f-b44d-9c670d206d2b@tnetconsulting.net>
 <alpine.BSF.2.02.1910272110350.28402@frieza.hoshinet.org>
 <xzdtvssnnqfvfxq9swgv979g@localhost>
 <alpine.BSF.2.02.1910281106290.77349@frieza.hoshinet.org>
 <alpine.BSF.2.21.9999.1910290544171.17400@aneurin.horsfall.org>
 <7wsgnc4rfd.fsf_-_@junk.nocrew.org>
 <CANCZdfqQ3gq=Wtee7XOG9yhuWf7bQEhCsw1xOEvE+JgByadHtw@mail.gmail.com>
Message-ID: <CAC0cEp8wNjc_PwxAr5qAq+htAgPTce7RXQehdNU=rhr=C0aLXQ@mail.gmail.com>

A bit off-off-topic, but as I mentioned elsewhere, I was lucky enough to
have one of the (if not *the*) first CRT terminals in the Labs. It was an
HP 264?, and it supported scrolling back to stored lines, and re-entering
them. I quickly settled in on a prompt that ended with "@", the default
"line kill", so whatever came before was ignored, and only the command that
followed was effectively re-entered. Quaint that "@" was a seldom-seen
character then.

I now have a prompt that ends with a newline. Still convenient for
copy/paste. The prompt itself has colors, separating host name from current
directory. This makes it easy to spot non-prompt line in the command line
history, and to determine which host I am connected to in that window, and
where I am on that host.


On Mon, Oct 28, 2019 at 3:57 PM Warner Losh <imp at bsdimp.com> wrote:

>
>
> On Mon, Oct 28, 2019 at 1:51 PM Lars Brinkhoff <lars at nocrew.org> wrote:
>
>> Dave Horsfall wrote:
>> > Steve Nickolas wrote:
>> >> 86-DOS actually did use ":" as a prompt character.
>> > The best I've ever seen was RT-11's "." - talk about minimalist...
>> >
>> > Actually this thread probably belongs on COFF by now.
>>
>> I was bound to happen.  List all the prompts!
>>
>> "*" seems popular on PDP-10s.
>>
>
> "@  " was the TOPS-20 prompt.
> "$ " was the VMS prompt
> RSTS/E was just "Ready\n"
>
> But none of these get us closer to CP/M's > prompt.
>
> Warner
> _______________________________________________
> COFF mailing list
> COFF at minnie.tuhs.org
> https://minnie.tuhs.org/cgi-bin/mailman/listinfo/coff
>
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://minnie.tuhs.org/pipermail/coff/attachments/20191028/c8ab0dc5/attachment.html>

From paul.winalski at gmail.com  Tue Oct 29 06:43:16 2019
From: paul.winalski at gmail.com (Paul Winalski)
Date: Mon, 28 Oct 2019 16:43:16 -0400
Subject: [COFF] [TUHS] UNIX Backslash History
In-Reply-To: <alpine.BSF.2.21.9999.1910290544171.17400@aneurin.horsfall.org>
References: <201910272031.x9RKVSem124842@tahoe.cs.Dartmouth.EDU>
 <CAFH29trMHepMHK0C+UapNVXvjfnFMv5ov4W4YS+yOn4i+mhV0A@mail.gmail.com>
 <CANV78LQa=VTZAMmVeRphTvrkxrkrAzgoU_-KqtdgqfWY2uUZJg@mail.gmail.com>
 <51f2d838-d097-a93f-b44d-9c670d206d2b@tnetconsulting.net>
 <alpine.BSF.2.02.1910272110350.28402@frieza.hoshinet.org>
 <xzdtvssnnqfvfxq9swgv979g@localhost>
 <alpine.BSF.2.02.1910281106290.77349@frieza.hoshinet.org>
 <alpine.BSF.2.21.9999.1910290544171.17400@aneurin.horsfall.org>
Message-ID: <CABH=_VReuYZSM4sTCss5gjcbEZm9rzTaKoA2z7D1QRnikMKE2w@mail.gmail.com>

On 10/28/19, Dave Horsfall <dave at horsfall.org> wrote:
> On Mon, 28 Oct 2019, Steve Nickolas wrote:
>
>> 86-DOS actually did use ":" as a prompt character.  This was changed for
>> IBM's release, for some clone releases, and for MS-DOS 2.0.
>
> The best I've ever seen was RT-11's "." - talk about minimalist...
>
> Actually this thread probably belongs on COFF by now.

RT-11 was following standard DEC practice by using "." as its command
prompt.  The "monitor dot" was the command prompt in both TOPS-10 and
TOPS-20.

Most DEC operating systems, including RT-11, TOPS-10/20, and VMS, used
"/" as a prefix on command options; "-" performs this function on UNIX
since "/" is the directory delimiter.  Back in the days of stand-alone
programs, physical switches on the console were used to set program
options.  This of course won't work when you have multiprogramming.  I
was told that DEC chose "/" because it looks like a toggle switch.
Command options in fact were initially called "switches".

-Paul W.

From krewat at kilonet.net  Tue Oct 29 09:13:54 2019
From: krewat at kilonet.net (Arthur Krewat)
Date: Mon, 28 Oct 2019 19:13:54 -0400
Subject: [COFF] Comparative promptology
In-Reply-To: <2daf68cb-1b16-05a5-af0c-6d64778b2da2@kilonet.net>
References: <201910272031.x9RKVSem124842@tahoe.cs.Dartmouth.EDU>
 <CAFH29trMHepMHK0C+UapNVXvjfnFMv5ov4W4YS+yOn4i+mhV0A@mail.gmail.com>
 <CANV78LQa=VTZAMmVeRphTvrkxrkrAzgoU_-KqtdgqfWY2uUZJg@mail.gmail.com>
 <51f2d838-d097-a93f-b44d-9c670d206d2b@tnetconsulting.net>
 <alpine.BSF.2.02.1910272110350.28402@frieza.hoshinet.org>
 <xzdtvssnnqfvfxq9swgv979g@localhost>
 <alpine.BSF.2.02.1910281106290.77349@frieza.hoshinet.org>
 <alpine.BSF.2.21.9999.1910290544171.17400@aneurin.horsfall.org>
 <7wsgnc4rfd.fsf_-_@junk.nocrew.org>
 <2daf68cb-1b16-05a5-af0c-6d64778b2da2@kilonet.net>
Message-ID: <c3c55842-dcb4-aae3-b1b4-695829e94ce9@kilonet.net>

Sorry, sent a picture along with this, but it got rejected because it 
was too big ;)

> On 10/28/2019 3:10 PM, Lars Brinkhoff wrote:
>> I was bound to happen. List all the prompts!
>> "*" seems popular on PDP-10s.
>
> Que? The only PDP-10 prompt that matters is "."
>
> The other less-desired (by me) is @
>
> art k.


From krewat at kilonet.net  Tue Oct 29 09:17:52 2019
From: krewat at kilonet.net (Arthur Krewat)
Date: Mon, 28 Oct 2019 19:17:52 -0400
Subject: [COFF] [TUHS] UNIX Backslash History
In-Reply-To: <CABH=_VReuYZSM4sTCss5gjcbEZm9rzTaKoA2z7D1QRnikMKE2w@mail.gmail.com>
References: <201910272031.x9RKVSem124842@tahoe.cs.Dartmouth.EDU>
 <CAFH29trMHepMHK0C+UapNVXvjfnFMv5ov4W4YS+yOn4i+mhV0A@mail.gmail.com>
 <CANV78LQa=VTZAMmVeRphTvrkxrkrAzgoU_-KqtdgqfWY2uUZJg@mail.gmail.com>
 <51f2d838-d097-a93f-b44d-9c670d206d2b@tnetconsulting.net>
 <alpine.BSF.2.02.1910272110350.28402@frieza.hoshinet.org>
 <xzdtvssnnqfvfxq9swgv979g@localhost>
 <alpine.BSF.2.02.1910281106290.77349@frieza.hoshinet.org>
 <alpine.BSF.2.21.9999.1910290544171.17400@aneurin.horsfall.org>
 <CABH=_VReuYZSM4sTCss5gjcbEZm9rzTaKoA2z7D1QRnikMKE2w@mail.gmail.com>
Message-ID: <62c3fa7e-1d77-3cbb-c6b7-f9cd48b3350c@kilonet.net>

On 10/28/2019 4:43 PM, Paul Winalski wrote:
> RT-11 was following standard DEC practice by using "." as its command
> prompt.  The "monitor dot" was the command prompt in both TOPS-10 and
> TOPS-20.

In my many accesses of TOPS-20 systems back in the day, thanks to the 
ARPANET, I am reasonably certain I never EVER once saw a TOPS-20 system 
with a "." prompt.

art k.

From lars at nocrew.org  Tue Oct 29 15:32:37 2019
From: lars at nocrew.org (Lars Brinkhoff)
Date: Tue, 29 Oct 2019 05:32:37 +0000
Subject: [COFF] Comparative promptology
In-Reply-To: <c3c55842-dcb4-aae3-b1b4-695829e94ce9@kilonet.net> (Arthur
 Krewat's message of "Mon, 28 Oct 2019 19:13:54 -0400")
References: <201910272031.x9RKVSem124842@tahoe.cs.Dartmouth.EDU>
 <CAFH29trMHepMHK0C+UapNVXvjfnFMv5ov4W4YS+yOn4i+mhV0A@mail.gmail.com>
 <CANV78LQa=VTZAMmVeRphTvrkxrkrAzgoU_-KqtdgqfWY2uUZJg@mail.gmail.com>
 <51f2d838-d097-a93f-b44d-9c670d206d2b@tnetconsulting.net>
 <alpine.BSF.2.02.1910272110350.28402@frieza.hoshinet.org>
 <xzdtvssnnqfvfxq9swgv979g@localhost>
 <alpine.BSF.2.02.1910281106290.77349@frieza.hoshinet.org>
 <alpine.BSF.2.21.9999.1910290544171.17400@aneurin.horsfall.org>
 <7wsgnc4rfd.fsf_-_@junk.nocrew.org>
 <2daf68cb-1b16-05a5-af0c-6d64778b2da2@kilonet.net>
 <c3c55842-dcb4-aae3-b1b4-695829e94ce9@kilonet.net>
Message-ID: <7wftjc3ymi.fsf@junk.nocrew.org>

Arthur Krewat wrote:
>> "*" seems popular on PDP-10s.
> Que? The only PDP-10 prompt that matters is "."

I mean programs like MACRO, LINK, etc.

From skogtun at gmail.com  Tue Oct 29 20:30:05 2019
From: skogtun at gmail.com (Harald Arnesen)
Date: Tue, 29 Oct 2019 11:30:05 +0100
Subject: [COFF] Comparative promptology
In-Reply-To: <CANCZdfqQ3gq=Wtee7XOG9yhuWf7bQEhCsw1xOEvE+JgByadHtw@mail.gmail.com>
References: <201910272031.x9RKVSem124842@tahoe.cs.Dartmouth.EDU>
 <CAFH29trMHepMHK0C+UapNVXvjfnFMv5ov4W4YS+yOn4i+mhV0A@mail.gmail.com>
 <CANV78LQa=VTZAMmVeRphTvrkxrkrAzgoU_-KqtdgqfWY2uUZJg@mail.gmail.com>
 <51f2d838-d097-a93f-b44d-9c670d206d2b@tnetconsulting.net>
 <alpine.BSF.2.02.1910272110350.28402@frieza.hoshinet.org>
 <xzdtvssnnqfvfxq9swgv979g@localhost>
 <alpine.BSF.2.02.1910281106290.77349@frieza.hoshinet.org>
 <alpine.BSF.2.21.9999.1910290544171.17400@aneurin.horsfall.org>
 <7wsgnc4rfd.fsf_-_@junk.nocrew.org>
 <CANCZdfqQ3gq=Wtee7XOG9yhuWf7bQEhCsw1xOEvE+JgByadHtw@mail.gmail.com>
Message-ID: <578c584e-0296-957f-955a-ccab0697560a@gmail.com>

Warner Losh [28.10.2019 20:57]:

> "@  " was the TOPS-20 prompt.

Also the Sintran (Norsk Data) prompt.
-- 
Hilsen Harald

From skogtun at gmail.com  Tue Oct 29 20:32:29 2019
From: skogtun at gmail.com (Harald Arnesen)
Date: Tue, 29 Oct 2019 11:32:29 +0100
Subject: [COFF] Comparative promptology
In-Reply-To: <578c584e-0296-957f-955a-ccab0697560a@gmail.com>
References: <201910272031.x9RKVSem124842@tahoe.cs.Dartmouth.EDU>
 <CAFH29trMHepMHK0C+UapNVXvjfnFMv5ov4W4YS+yOn4i+mhV0A@mail.gmail.com>
 <CANV78LQa=VTZAMmVeRphTvrkxrkrAzgoU_-KqtdgqfWY2uUZJg@mail.gmail.com>
 <51f2d838-d097-a93f-b44d-9c670d206d2b@tnetconsulting.net>
 <alpine.BSF.2.02.1910272110350.28402@frieza.hoshinet.org>
 <xzdtvssnnqfvfxq9swgv979g@localhost>
 <alpine.BSF.2.02.1910281106290.77349@frieza.hoshinet.org>
 <alpine.BSF.2.21.9999.1910290544171.17400@aneurin.horsfall.org>
 <7wsgnc4rfd.fsf_-_@junk.nocrew.org>
 <CANCZdfqQ3gq=Wtee7XOG9yhuWf7bQEhCsw1xOEvE+JgByadHtw@mail.gmail.com>
 <578c584e-0296-957f-955a-ccab0697560a@gmail.com>
Message-ID: <fe823dd3-8b5a-4a4f-1705-1881e055b5ca@gmail.com>

Harald Arnesen [29.10.2019 11:30]:
> Warner Losh [28.10.2019 20:57]:
> 
>> "@  " was the TOPS-20 prompt.
> 
> Also the Sintran (Norsk Data) prompt.

btw, we used to call it "grisehale" ("pig's tail").
-- 
Hilsen Harald

From michael at kjorling.se  Wed Oct 30 05:57:22 2019
From: michael at kjorling.se (Michael =?utf-8?B?S2rDtnJsaW5n?=)
Date: Tue, 29 Oct 2019 19:57:22 +0000
Subject: [COFF] Comparative promptology
In-Reply-To: <CANCZdfqQ3gq=Wtee7XOG9yhuWf7bQEhCsw1xOEvE+JgByadHtw@mail.gmail.com>
References: <201910272031.x9RKVSem124842@tahoe.cs.Dartmouth.EDU>
 <CAFH29trMHepMHK0C+UapNVXvjfnFMv5ov4W4YS+yOn4i+mhV0A@mail.gmail.com>
 <CANV78LQa=VTZAMmVeRphTvrkxrkrAzgoU_-KqtdgqfWY2uUZJg@mail.gmail.com>
 <51f2d838-d097-a93f-b44d-9c670d206d2b@tnetconsulting.net>
 <alpine.BSF.2.02.1910272110350.28402@frieza.hoshinet.org>
 <xzdtvssnnqfvfxq9swgv979g@localhost>
 <alpine.BSF.2.02.1910281106290.77349@frieza.hoshinet.org>
 <alpine.BSF.2.21.9999.1910290544171.17400@aneurin.horsfall.org>
 <7wsgnc4rfd.fsf_-_@junk.nocrew.org>
 <CANCZdfqQ3gq=Wtee7XOG9yhuWf7bQEhCsw1xOEvE+JgByadHtw@mail.gmail.com>
Message-ID: <n7pzf4nf9hz9tbgp4gcndp7b@localhost>

On 28 Oct 2019 13:57 -0600, from imp at bsdimp.com (Warner Losh):
> "@  " was the TOPS-20 prompt.
> "$ " was the VMS prompt
> RSTS/E was just "Ready\n"
> 
> But none of these get us closer to CP/M's > prompt.

For micros, the Apple II used "]", didn't it?

That's not exactly a ">" either, but it's somewhat period- and system
size-appropriate for early CP/M.

-- 
Michael Kjörling • https://michael.kjorling.se • michael at kjorling.se
  “The most dangerous thought that you can have as a creative person
              is to think you know what you’re doing.” (Bret Victor)


From lars at nocrew.org  Wed Oct 30 17:01:52 2019
From: lars at nocrew.org (Lars Brinkhoff)
Date: Wed, 30 Oct 2019 07:01:52 +0000
Subject: [COFF] Comparative promptology
In-Reply-To: <CANCZdfqQ3gq=Wtee7XOG9yhuWf7bQEhCsw1xOEvE+JgByadHtw@mail.gmail.com>
 (Warner Losh's message of "Mon, 28 Oct 2019 13:57:01 -0600")
References: <201910272031.x9RKVSem124842@tahoe.cs.Dartmouth.EDU>
 <CAFH29trMHepMHK0C+UapNVXvjfnFMv5ov4W4YS+yOn4i+mhV0A@mail.gmail.com>
 <CANV78LQa=VTZAMmVeRphTvrkxrkrAzgoU_-KqtdgqfWY2uUZJg@mail.gmail.com>
 <51f2d838-d097-a93f-b44d-9c670d206d2b@tnetconsulting.net>
 <alpine.BSF.2.02.1910272110350.28402@frieza.hoshinet.org>
 <xzdtvssnnqfvfxq9swgv979g@localhost>
 <alpine.BSF.2.02.1910281106290.77349@frieza.hoshinet.org>
 <alpine.BSF.2.21.9999.1910290544171.17400@aneurin.horsfall.org>
 <7wsgnc4rfd.fsf_-_@junk.nocrew.org>
 <CANCZdfqQ3gq=Wtee7XOG9yhuWf7bQEhCsw1xOEvE+JgByadHtw@mail.gmail.com>
Message-ID: <7wzhhi1ztr.fsf@junk.nocrew.org>

Warner Losh wrote:
> But none of these get us closer to CP/M's > prompt.

To me > suggests an arrow indicating "enter your input here".  Pure
speculation of course.

Interestingly, some ITS programs does the exact opposite.  They use the
1963 ASCII character "left arrow" as a prompt.  (Today it just outputs
as an underscore but printed texts and screenshots show that the 1963
character set was in use.)

From tih at hamartun.priv.no  Wed Oct 30 18:19:20 2019
From: tih at hamartun.priv.no (Tom Ivar Helbekkmo)
Date: Wed, 30 Oct 2019 09:19:20 +0100
Subject: [COFF] Comparative promptology
In-Reply-To: <fe823dd3-8b5a-4a4f-1705-1881e055b5ca@gmail.com> (Harald
 Arnesen's message of "Tue, 29 Oct 2019 11:32:29 +0100")
References: <201910272031.x9RKVSem124842@tahoe.cs.Dartmouth.EDU>
 <CAFH29trMHepMHK0C+UapNVXvjfnFMv5ov4W4YS+yOn4i+mhV0A@mail.gmail.com>
 <CANV78LQa=VTZAMmVeRphTvrkxrkrAzgoU_-KqtdgqfWY2uUZJg@mail.gmail.com>
 <51f2d838-d097-a93f-b44d-9c670d206d2b@tnetconsulting.net>
 <alpine.BSF.2.02.1910272110350.28402@frieza.hoshinet.org>
 <xzdtvssnnqfvfxq9swgv979g@localhost>
 <alpine.BSF.2.02.1910281106290.77349@frieza.hoshinet.org>
 <alpine.BSF.2.21.9999.1910290544171.17400@aneurin.horsfall.org>
 <7wsgnc4rfd.fsf_-_@junk.nocrew.org>
 <CANCZdfqQ3gq=Wtee7XOG9yhuWf7bQEhCsw1xOEvE+JgByadHtw@mail.gmail.com>
 <578c584e-0296-957f-955a-ccab0697560a@gmail.com>
 <fe823dd3-8b5a-4a4f-1705-1881e055b5ca@gmail.com>
Message-ID: <m2bltyzlvb.fsf@thuvia.hamartun.priv.no>

Harald Arnesen <skogtun at gmail.com> writes:

> Harald Arnesen [29.10.2019 11:30]:
>> Warner Losh [28.10.2019 20:57]:
>> 
>>> "@  " was the TOPS-20 prompt.
>> 
>> Also the Sintran (Norsk Data) prompt.
>
> btw, we used to call it "grisehale" ("pig's tail").

Not at the Norwegian Institute of Technology (now part of the Norwegian
University of Technology and Science).  There, it was called "nabla",
because of the EXEC 8 operating system on UNIVAC mainframes, which used
the FIELDATA character set, and where the "Master Space" character,
(visually represented by nabla, which looks like an upside-down capital
delta: '∇' if what you're reading this text on supports Unicode) was
used as a prefix character indicating an operating system command.  It
was mapped to ASCII 64 externally, a natural choice, because FIELDATA
had no @, ASCII had no nabla -- and Master Space was encoded as decimal
64 in FIELDATA already.

The name stuck, and @ kept being called nabla at least until 1990.

-tih
-- 
Most people who graduate with CS degrees don't understand the significance
of Lisp.  Lisp is the most important idea in computer science.  --Alan Kay

From grog at lemis.com  Thu Oct 31 11:19:05 2019
From: grog at lemis.com (Greg 'groggy' Lehey)
Date: Thu, 31 Oct 2019 12:19:05 +1100
Subject: [COFF] Fieldata characters (was:  Comparative promptology)
In-Reply-To: <m2bltyzlvb.fsf@thuvia.hamartun.priv.no>
References: <51f2d838-d097-a93f-b44d-9c670d206d2b@tnetconsulting.net>
 <alpine.BSF.2.02.1910272110350.28402@frieza.hoshinet.org>
 <xzdtvssnnqfvfxq9swgv979g@localhost>
 <alpine.BSF.2.02.1910281106290.77349@frieza.hoshinet.org>
 <alpine.BSF.2.21.9999.1910290544171.17400@aneurin.horsfall.org>
 <7wsgnc4rfd.fsf_-_@junk.nocrew.org>
 <CANCZdfqQ3gq=Wtee7XOG9yhuWf7bQEhCsw1xOEvE+JgByadHtw@mail.gmail.com>
 <578c584e-0296-957f-955a-ccab0697560a@gmail.com>
 <fe823dd3-8b5a-4a4f-1705-1881e055b5ca@gmail.com>
 <m2bltyzlvb.fsf@thuvia.hamartun.priv.no>
Message-ID: <20191031011905.GA68439@eureka.lemis.com>

On Wednesday, 30 October 2019 at  9:19:20 +0100, COFF wrote:
> Harald Arnesen <skogtun at gmail.com> writes:
>
>> Harald Arnesen [29.10.2019 11:30]:
>>> Warner Losh [28.10.2019 20:57]:
>>>
>>>> "@Â Â " was the TOPS-20 prompt.
>>>
>>> Also the Sintran (Norsk Data) prompt.
>>
>> btw, we used to call it "grisehale" ("pig's tail").
>
> Not at the Norwegian Institute of Technology (now part of the Norwegian
> University of Technology and Science).  There, it was called "nabla",
> because of the EXEC 8 operating system on UNIVAC mainframes, which used
> the FIELDATA character set, and where the "Master Space" character,
> (visually represented by nabla, which looks like an upside-down capital
> delta: '???' if what you're reading this text on supports Unicode) was
> used as a prefix character indicating an operating system command.

I worked for and with UNIVAC for most of the 1970s and early 1980s,
including on EXEC 8/OS 1100, and the master space (binary 0) was
always represented by @, on punched cards, printouts, terminals and
the documentation.  I've just confirmed with my copy of UP-4040,
dating from 1971.  https://en.wikipedia.org/wiki/Fieldata#UNIVAC
agrees, though it notes:

   Sometimes switched with Î

But that's a delta, not a nabla.  Fieldata also had a Î (code 04), and
I have never seen this switch.

FWIW, this was in Germany, where we called the @ a âKlammeraffeâ
(originally a spider monkey).  This wasn't limited to UNIVAC.

Greg
--
Sent from my desktop computer.
Finger grog at lemis.com for PGP public key.
See complete headers for address and phone numbers.
This message is digitally signed.  If your Microsoft mail program
reports problems, please read http://lemis.com/broken-MUA
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 163 bytes
Desc: not available
URL: <http://minnie.tuhs.org/pipermail/coff/attachments/20191031/429d998a/attachment.sig>

From gtaylor at tnetconsulting.net  Thu Oct 31 11:59:00 2019
From: gtaylor at tnetconsulting.net (Grant Taylor)
Date: Wed, 30 Oct 2019 19:59:00 -0600
Subject: [COFF] [TUHS] UNIX Backslash History
In-Reply-To: <CABH=_VReuYZSM4sTCss5gjcbEZm9rzTaKoA2z7D1QRnikMKE2w@mail.gmail.com>
References: <201910272031.x9RKVSem124842@tahoe.cs.Dartmouth.EDU>
 <CAFH29trMHepMHK0C+UapNVXvjfnFMv5ov4W4YS+yOn4i+mhV0A@mail.gmail.com>
 <CANV78LQa=VTZAMmVeRphTvrkxrkrAzgoU_-KqtdgqfWY2uUZJg@mail.gmail.com>
 <51f2d838-d097-a93f-b44d-9c670d206d2b@tnetconsulting.net>
 <alpine.BSF.2.02.1910272110350.28402@frieza.hoshinet.org>
 <xzdtvssnnqfvfxq9swgv979g@localhost>
 <alpine.BSF.2.02.1910281106290.77349@frieza.hoshinet.org>
 <alpine.BSF.2.21.9999.1910290544171.17400@aneurin.horsfall.org>
 <CABH=_VReuYZSM4sTCss5gjcbEZm9rzTaKoA2z7D1QRnikMKE2w@mail.gmail.com>
Message-ID: <1a82938e-09c0-05d8-8066-5f5f270281e2@spamtrap.tnetconsulting.net>

On 10/28/19 2:43 PM, Paul Winalski wrote:
rrrrrrrrrrr> Back in the days of stand-alone programs, physical switches 
on the
> console were used to set program options.

This helps explain why the option is called a "switch" in some OSs.



-- 
Grant. . . .
unix || die

-------------- next part --------------
A non-text attachment was scrubbed...
Name: smime.p7s
Type: application/pkcs7-signature
Size: 4008 bytes
Desc: S/MIME Cryptographic Signature
URL: <http://minnie.tuhs.org/pipermail/coff/attachments/20191030/717374ea/attachment.bin>

