Welcome to this complete Australian Unix Users Group (AUUG) newsletter
collection, spanning over 25 years from October 1978 through to June 2005.
Most of the scanned AUUGNs here come from Warren Toomey's collection,
except for the following:

  - John Dodgson's scans for V01.1 ... V03.6, which came
    from http://www.physiol.usyd.edu.au/johnd/auug

  - Steve Jenkin donated the following issues for scanning:
    V05.2, V05.3, V05.4, V08.5, V11.2, V19.4, V20.3, V20.4,
    V21.2, V21.3, V25.1 ... V25.4

  - Frank Crawford donated the following issues for scanning:
    V09.5, V10.5, V10.6, V11.1, V11.3, V11.4, V12.1, V12.4/5,
    V12.6, V13.1, V13.3, V13.6, V14.1, V14.2

  - David Purdue donated the following issues for scanning:
    V09.2, V09.3, V12.1, V12.2/3, V13.2, V13.4

Publication Dates
-----------------
V01.1	Oct 1978	V13.1	Feb 1992
V01.2	Dec 1978	V13.2	Apr 1992
V01.3	Feb 1979	V13.3	Jun 1992
V01.4	Apr 1979	V13.4	Aug 1992
V01.5	Jun 1979	V13.5	Oct 1992
V01.5	Aug 1979	V13.6	Dec 1992
V02.1	Oct 1979	V14.1	Feb 1993
V02.2	Dec 1979	V14.2	Apr 1993
V02.3	Feb 1980	V14.3	Jun 1993
V02.4	Apr 1980	V14.4	Aug 1993
V02.5	Jun 1980	V14.5	Oct 1993
V02.6	Aug 1980	V14.6	Dec 1993
V03.1	Oct 1980	V15.1	Feb 1994
V03.2	Dec 1980	V15.2	Apr 1994
V03.3	Feb 1981	V15.3	Jun 1994
V03.4	Apr 1981	V15.4	Aug 1994
V03.5	Jun 1981	V15.5	Oct 1994
V03.6	Aug 1981	V15.6	Dec 1994
V04.1	Jan 1982	V16.1	Feb 1995
V04.2	Mar 1982	V16.2	Apr 1995
V04.3	May 1982	V16.3	Jun 1995
V04.4	Jul 1982	V16.4	Aug 1995
V04.5	Sep 1982	V16.5	Oct 1995
V04.6	Nov 1982	V16.6	Dec 1995
V05.1	Feb 1984	V17.1	Feb 1996
V05.2	Mar 1984	V17.2	Apr 1996
V05.3	Jun 1984	V17.3	Jun 1996
V05.4	Jul 1984	V17.4	Aug 1996
V05.5	Oct 1984	V17.56	Oct 1996
V05.6	Jan 1985	V18.1	Feb 1997
V06.1	May 1985	V18.2	May 1997
V06.2	Jul 1985	V18.3	Aug 1997
V06.3	Oct 1985	V18.4	Dec 1997
V06.4	Jan 1986	V19.1	Feb 1998
V06.5	Apr 1986	V19.2	May 1998
V06.6	Aug 1986	V19.3	Aug 1998
V07.1	Oct 1986	V19.4	Nov 1998
V07.23	Dec 1986	V20.1	Feb 1999
V07.45	Feb 1987	V20.2	Jun 1999
V07.6	Apr 1987	V20.3	Aug 1999
V08.12	Jun 1987	V20.4	Nov 1999
V08.34	Aug 1987	V21.1	Mar 2000
V08.5	Oct 1987	V21.2	Jun 2000
V08.6	Dec 1987	V21.3	Sep 2000
V09.1	Feb 1988	V21.4	Dec 2000
V09.2	Apr 1988	V22.1	Mar 2001
V09.3	Jun 1988	V22.2	Jun 2001
V09.4	Sep 1988	V22.3	Nov 2001
V09.5	Oct 1988	V22.4	Dec 2001
V09.6	Dec 1988	V23.1	Mar 2002
V10.1	Feb 1989	V23.2	Jul 2002
V10.2	Apr 1989	V23.3	Oct 2002
V10.3	Jun 1989	V23.4	Dec 2002
V10.4	Aug 1989	V24.1	Mar 2003
V10.5	Oct 1989	V24.2	Jun 2003
V10.6	Dec 1989	V24.3	Sep 2003
V11.1	Feb 1990	V24.4	Dec 2003
V11.2	Apr 1990	V25.1	Mar 2004
V11.3	Jun 1990	V25.2	Jun 2004
V11.4	Dec 1990	V25.3	Sep 2004
V12.1	Apr 1991	V25.4	Dec 2004
V12.23	Aug 1991	V26.1	Jul 2005
V12.45	Oct 1991	
V12.6	Dec 1991	
