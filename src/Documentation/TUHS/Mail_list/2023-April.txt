From tuhs at tuhs.org  Sat Apr  1 04:13:38 2023
From: tuhs at tuhs.org (segaloco via TUHS)
Date: Fri, 31 Mar 2023 18:13:38 +0000
Subject: [TUHS] Paper Scan of Research V4 Manual?
Message-ID: <GCS9zwolt401XAvrSL183sVQfDqOvTBG8lWHK2uuq0MxIqacde-R3zIGpQnslTQL__ivCxkTfOXrJbQqPKXszi46nKjJsFvGHSSoUXbaq-4=@protonmail.com>

Good afternoon, folks.  I was wondering if anyone is aware of/possesses a scanned copy (or paper copy ripe for scanning) of the research V4 UNIX Programmer's Manual.  I've found a few rendered PDFs from the available manpage sources, but I am looking to do some comparison of original typesetting re: my restorations of various other sets of manpages.  I have scans of all the other research versions I believe, just not V4.  Thanks in advance!

- Matt G.

From ron at ronnatalie.com  Sun Apr  2 02:34:17 2023
From: ron at ronnatalie.com (Ron Natalie)
Date: Sat, 01 Apr 2023 16:34:17 +0000
Subject: [TUHS] Warning: April Fools
Message-ID: <em1be8dba3-c3b7-4aaa-be88-f6ab071b6e45@ad99ed46.com>

Once again, I must dredge up this post from 1991….


>From spaf at cs.purdue.EDU Thu Apr  4 23:11:22 1991
Path: ai-lab!mintaka!mit-eddie!wuarchive!usc!apple!amdahl!walldrug!moscvax!perdue!spaf
From:
spaf at cs.purdue.EDU (Gene Spafford)
Newsgroups: news.announce.important,news.admin
Subject: Warning: April Fools Time again (forged messages on the loose!)
Message-ID: <
4-1-1991 at medusa.cs.purdue.edu>
Date: 1 Apr 91 00:00:00 GMT
Expires: 1 May 91 00:00:00 GMT
Followup-To: news.admin
Organization: Dept. of Computer Sciences, Purdue Univ.
Lines: 25
Approved:
spaf at cs.purdue.EDU
Xref: ai-lab news.announce.important:19 news.admin:8235

Warning: April 1 is rapidly approaching, and with it comes a USENET
tradition. On April Fools day comes a series of forged, tongue-in-cheek
messages, either from non-existent sites or using the name of a Well Known
USENET person. In general, these messages are harmless and meant as a joke,
and people who respond to these messages without thinking, either by flaming
or otherwise responding, generally end up looking rather silly when the
forgery is exposed.

So, for the few weeks, if you see a message that seems completely out
of line or is otherwise unusual, think twice before posting a followup
or responding to it; it's very likely a forgery.

There are a few ways of checking to see if a message is a forgery. These
aren't foolproof, but since most forgery posters want people to figure it
out, they will allow you to track down the vast majority of forgeries:

	o Russian computers. For historic reasons most forged messages have
	  as part of their Path: a non-existent (we think!) russian
	  computer, either kremvax or moscvax. Other possibilities are
	  nsacyber or wobegon. Please note, however, that walldrug is a real
	  site and isn't a forgery.

	o Posted dates. Almost invariably, the date of the posting is forged
	  to be April 1.

	o Funky Message-ID. Subtle hints are often lodged into the
	  Message-Id, as that field is more or less an unparsed text string
	  and can contain random information. Common values include pi,
	  the phone number of the red phone in the white house, and the
	  name of the forger's parrot.

	o subtle mispellings. Look for subtle misspellings of the host names
	  in the Path: field when a message is forged in the name of a Big
	  Name USENET person. This is done so that the person being forged
	  actually gets a chance to see the message and wonder when he
	  actually posted it.

Forged messages, of course, are not to be condoned. But they happen, and
it's important for people on the net not to over-react. They happen at this
time every year, and the forger generally gets their kick from watching the
novice users take the posting seriously and try to flame their tails off. If
we can keep a level head and not react to these postings, they'll taper off
rather quickly and we can return to the normal state of affairs: chaos.

Thanks for your support.

Gene Spafford, Net.God (and probably tired of seeing this message)
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://www.tuhs.org/pipermail/tuhs/attachments/20230401/8415f992/attachment.htm>

From cowan at ccil.org  Sun Apr  2 03:55:15 2023
From: cowan at ccil.org (John Cowan)
Date: Sat, 1 Apr 2023 13:55:15 -0400
Subject: [TUHS] Warning: April Fools
In-Reply-To: <em1be8dba3-c3b7-4aaa-be88-f6ab071b6e45@ad99ed46.com>
References: <em1be8dba3-c3b7-4aaa-be88-f6ab071b6e45@ad99ed46.com>
Message-ID: <CAD2gp_TjDavZwXPuzOnaBodd5jYiUQe608AQ2Ho56mzhQPLrew@mail.gmail.com>

See also <https://godfatherof.nl/kremvax.html>.

Yesterday, indeed, I got an obviously misdirected email on my work account,
I clicked on a link in it to try to figure out what had gone wrong, and
found myself on a page informing me that $EMPLOYER had just phished me, and
I was being sent to a re-education camp (well, in fact I just had to look
at some slides and answer questions).  Of course, it's very difficult to
train people out of being careless and impulsive.

-- 

John Cowan          {backbones}!rutgers!hombre!magpie!cowan
                Charles li reis, nostre emperesdre magnes,
                Set anz totz pleinz ad ested in Espagnes.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://www.tuhs.org/pipermail/tuhs/attachments/20230401/e2852ff9/attachment.htm>

From sjenkin at canb.auug.org.au  Sun Apr  2 10:08:24 2023
From: sjenkin at canb.auug.org.au (steve jenkin)
Date: Sun, 2 Apr 2023 10:08:24 +1000
Subject: [TUHS] Warning: April Fools
In-Reply-To: <em1be8dba3-c3b7-4aaa-be88-f6ab071b6e45@ad99ed46.com>
References: <em1be8dba3-c3b7-4aaa-be88-f6ab071b6e45@ad99ed46.com>
Message-ID: <73FC4C4C-7479-4747-A528-3A49BB62752C@canb.auug.org.au>

[ Please post follow-ups to COFF ]

Ron,

Thanks for the history, enjoyed very much.
Quite relevant to Early Unix, intertwined with VAxen, IP stack from UCB, NSF-net & fakery.

The earliest documented Trojan, Unix or not, would be Ken’s login/cc hack in his “Reflections on Trust” paper.

It was 1986 when Clifford Stoll tracked a KGB recruit who broke into MILNET, then the first “honeynet” by Stoll.

	<https://en.wikipedia.org/wiki/Clifford_Stoll#Career>
	<https://en.wikipedia.org/wiki/The_Cuckoo%27s_Egg_(book)>

1986 was also the first known PC virus according to Kaspersky.

	<https://www.kaspersky.com.au/resource-center/threats/a-brief-history-of-computer-viruses-and-what-the-future-holds?
	“Brain (boot) , the first PC virus, began infecting 5.2" floppy disks in 1986.”

2nd November 1988, the Morris worm escaped from a lab,
& overloaded the Internet for a week.

Causing CERT to be formed in November 1988 in response.
	<https://en.wikipedia.org/wiki/CERT_Coordination_Center>

The SANS Institute was formed the next year, 1989, creating structured training & security materials.
	<https://en.wikipedia.org/wiki/SANS_Institute>

This structured, co-ordinated response, led by technical folk, not NatSec/ Intelligence/ Criminal investigation bodies,
created CVE’s, Common Vulnerabilities and Exposures, as a way to identify & name
unique attacks & vectors, track them and make vendors aware, forcing publicity & responses.

	<https://en.wikipedia.org/wiki/Common_Vulnerabilities_and_Exposures>
	<https://cve.mitre.org>

The Internet eventually became a significant theatre of Crime & Espionage, Commercial & National Security.

Mandiant was formed in 2004 to identify, track and find sources of APT’s, Advanced Persistent Threats.
In 2010, they described APT’s  tracked in their “M-trends” newsletter.
in Feb 2013, Mandiant publicly described “APT1” and the military unit & location they believed ran it.

	<https://en.wikipedia.org/wiki/Mandiant>
	<https://en.wikipedia.org/wiki/Advanced_persistent_threat>
	<https://www.lawfareblog.com/mandiant-report-apt1>
	<https://www.mandiant.com/resources/blog/mandiant-exposes-apt1-chinas-cyber-espionage-units>

=============

> On 2 Apr 2023, at 02:34, Ron Natalie <ron at ronnatalie.com> wrote:
> 
> Once again, I must dredge up this post from 1991….

=============

For future reference, Kremvax lives! [ datestamp in email header ]

iMac1:steve$ host kremvax.demos.su
	kremvax.demos.su has address 194.87.0.20
	kremvax.demos.su mail is handled by 100 relay2.demos.su.
	kremvax.demos.su mail is handled by 50 relay1.demos.su.

iMac1:steve$ ping -c2 kremvax.demos.su
	PING kremvax.demos.su (194.87.0.20): 56 data bytes
	64 bytes from 194.87.0.20: icmp_seq=0 ttl=46 time=336.127 ms
	64 bytes from 194.87.0.20: icmp_seq=1 ttl=46 time=335.823 ms

--- kremvax.demos.su ping statistics ---
2 packets transmitted, 2 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 335.823/335.975/336.127/0.152 ms

=============

--
Steve Jenkin, IT Systems and Design 
0412 786 915 (+61 412 786 915)
PO Box 38, Kippax ACT 2615, AUSTRALIA

mailto:sjenkin at canb.auug.org.au http://members.tip.net.au/~sjenkin


From noel.hunt at gmail.com  Sun Apr  2 17:39:32 2023
From: noel.hunt at gmail.com (Noel Hunt)
Date: Sun, 2 Apr 2023 17:39:32 +1000
Subject: [TUHS] Warning: April Fools
In-Reply-To: <CAD2gp_TjDavZwXPuzOnaBodd5jYiUQe608AQ2Ho56mzhQPLrew@mail.gmail.com>
References: <em1be8dba3-c3b7-4aaa-be88-f6ab071b6e45@ad99ed46.com>
 <CAD2gp_TjDavZwXPuzOnaBodd5jYiUQe608AQ2Ho56mzhQPLrew@mail.gmail.com>
Message-ID: <CAGfO01y15ECtvirSnTgKba9C201KkXuPYjCF6mf=ypFy67hSvA@mail.gmail.com>

 Charles li reis, nostre emperesdre magnes, Set anz totz pleinz ad ested in
Espagnes.

A translation would be most helpful. It looks like a mixture
of Spanish and Mediaevel French...ah, it is the La Chanson de
Roland.


On Sun, 2 Apr 2023 at 03:55, John Cowan <cowan at ccil.org> wrote:

> See also <https://godfatherof.nl/kremvax.html>.
>
> Yesterday, indeed, I got an obviously misdirected email on my work
> account, I clicked on a link in it to try to figure out what had gone
> wrong, and found myself on a page informing me that $EMPLOYER had just
> phished me, and I was being sent to a re-education camp (well, in fact I
> just had to look at some slides and answer questions).  Of course, it's
> very difficult to train people out of being careless and impulsive.
>
> --
>
> John Cowan          {backbones}!rutgers!hombre!magpie!cowan
>                 Charles li reis, nostre emperesdre magnes,
>                 Set anz totz pleinz ad ested in Espagnes.
>
>
>
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://www.tuhs.org/pipermail/tuhs/attachments/20230402/dc99abb0/attachment.htm>

From wobblygong at gmail.com  Sun Apr  2 21:18:12 2023
From: wobblygong at gmail.com (Wesley Parish)
Date: Sun, 2 Apr 2023 23:18:12 +1200
Subject: [TUHS] Warning: April Fools
In-Reply-To: <CAGfO01y15ECtvirSnTgKba9C201KkXuPYjCF6mf=ypFy67hSvA@mail.gmail.com>
References: <em1be8dba3-c3b7-4aaa-be88-f6ab071b6e45@ad99ed46.com>
 <CAD2gp_TjDavZwXPuzOnaBodd5jYiUQe608AQ2Ho56mzhQPLrew@mail.gmail.com>
 <CAGfO01y15ECtvirSnTgKba9C201KkXuPYjCF6mf=ypFy67hSvA@mail.gmail.com>
Message-ID: <2148ce0c-e49d-8789-d632-1c3ec27698e8@gmail.com>

"Charles the King, our great emperor"

(and the rest I can only guess at. It's referring to King Chuck the 
Great, Karl der Gross, Charlemagne. Or if you like, the Great and 
Magnificent King Upchuck. :) ) It's not Spanish; I thought it had a 
touch of Occitan/Catalan, but I could be wrong.

Wesley Parish

On 2/04/23 19:39, Noel Hunt wrote:
> Charles li reis, nostre emperesdre magnes, Set anz totz pleinz ad 
> ested in Espagnes.
> A translation would be most helpful. It looks like a mixture
> of Spanish and Mediaevel French...ah, it is the La Chanson de
> Roland.
>
> On Sun, 2 Apr 2023 at 03:55, John Cowan <cowan at ccil.org> wrote:
>
>     See also <https://godfatherof.nl/kremvax.html>.
>
>     Yesterday, indeed, I got an obviously misdirected email on my work
>     account, I clicked on a link in it to try to figure out what had
>     gone wrong, and found myself on a page informing me that $EMPLOYER
>     had just phished me, and I was being sent to a re-education camp
>     (well, in fact I just had to look at some slides and answer
>     questions). Of course, it's very difficult to train people out of
>     being careless and impulsive.
>
>     -- 
>
>     John Cowan          {backbones}!rutgers!hombre!magpie!cowan
>                      Charles li reis, nostre emperesdre magnes,
>                      Set anz totz pleinz ad ested in Espagnes.
>
>
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://www.tuhs.org/pipermail/tuhs/attachments/20230402/51eac2d8/attachment-0001.htm>

From meillo at marmaro.de  Sun Apr  2 22:29:03 2023
From: meillo at marmaro.de (markus schnalke)
Date: Sun, 02 Apr 2023 14:29:03 +0200
Subject: [TUHS] Warning: April Fools
In-Reply-To: <2148ce0c-e49d-8789-d632-1c3ec27698e8@gmail.com>
References: <em1be8dba3-c3b7-4aaa-be88-f6ab071b6e45@ad99ed46.com>
 <CAD2gp_TjDavZwXPuzOnaBodd5jYiUQe608AQ2Ho56mzhQPLrew@mail.gmail.com>
 <CAGfO01y15ECtvirSnTgKba9C201KkXuPYjCF6mf=ypFy67hSvA@mail.gmail.com>
 <2148ce0c-e49d-8789-d632-1c3ec27698e8@gmail.com>
Message-ID: <1piwpT-8E5-00@marmaro.de>

Hoi,

As Noel already mentioned, it's from the Song of Roland. See here,
the beginning of this old french version:

https://fr.wikisource.org/wiki/La_Chanson_de_Roland/L%C3%A9on_Gautier/%C3%89dition_critique/Premi%C3%A8re_partie


meillo



[2023-04-02 23:18] Wesley Parish <wobblygong at gmail.com>
>
> "Charles the King, our great emperor"
> 
> (and the rest I can only guess at. It's referring to King Chuck the Great, Karl
> der Gross, Charlemagne. Or if you like, the Great and Magnificent King Upchuck.
> :) ) It's not Spanish; I thought it had a touch of Occitan/Catalan, but I could
> be wrong.
> 
> Wesley Parish
> 
> On 2/04/23 19:39, Noel Hunt wrote:
> 
>     Charles li reis, nostre emperesdre magnes, Set anz totz pleinz ad ested in
>     Espagnes.
>     A translation would be most helpful. It looks like a mixture
>     of Spanish and Mediaevel French...ah, it is the La Chanson de
>     Roland.
>    
>     On Sun, 2 Apr 2023 at 03:55, John Cowan <cowan at ccil.org> wrote:
> 
>         See also <https://godfatherof.nl/kremvax.html>.
> 
>         Yesterday, indeed, I got an obviously misdirected email on my work
>         account, I clicked on a link in it to try to figure out what had gone
>         wrong, and found myself on a page informing me that $EMPLOYER had just
>         phished me, and I was being sent to a re-education camp (well, in fact
>         I just had to look at some slides and answer questions).  Of course,
>         it's very difficult to train people out of being careless and
>         impulsive.
> 
>         -- 
> 
>         John Cowan          {backbones}!rutgers!hombre!magpie!cowan
>                         Charles li reis, nostre emperesdre magnes,
>                         Set anz totz pleinz ad ested in Espagnes.
> 
> 
> 

From skogtun at gmail.com  Sun Apr  2 23:46:20 2023
From: skogtun at gmail.com (Harald Arnesen)
Date: Sun, 2 Apr 2023 15:46:20 +0200
Subject: [TUHS] Warning: April Fools
In-Reply-To: <2148ce0c-e49d-8789-d632-1c3ec27698e8@gmail.com>
References: <em1be8dba3-c3b7-4aaa-be88-f6ab071b6e45@ad99ed46.com>
 <CAD2gp_TjDavZwXPuzOnaBodd5jYiUQe608AQ2Ho56mzhQPLrew@mail.gmail.com>
 <CAGfO01y15ECtvirSnTgKba9C201KkXuPYjCF6mf=ypFy67hSvA@mail.gmail.com>
 <2148ce0c-e49d-8789-d632-1c3ec27698e8@gmail.com>
Message-ID: <2f3bf40b-af75-f899-58a1-611b8c52a8c7@gmail.com>

Wesley Parish [02/04/2023 13.18]:

> It's referring to King Chuck the Great, Karl der Gross, Charlemagne. Or 
> if you like, the Great and Magnificent King Upchuck. :) )

Karlamagnus, as the vikings called him.
-- 
Hilsen Harald
Слава Україні!


From tuhs at tuhs.org  Mon Apr  3 15:03:56 2023
From: tuhs at tuhs.org (segaloco via TUHS)
Date: Mon, 03 Apr 2023 05:03:56 +0000
Subject: [TUHS] Manpage Analysis Repository (And V1/V2 roff Restorations)
Message-ID: <htluRlWC_ONMqHYtakpSVnz_LliMjaP9pVjvk5W8Q7Kzn3WDTvnQak5INcm51udRvY0rTGh0KiHLVgoWm0P_A1lEmVFCEDm2B9S3YPB9aVA=@protonmail.com>

Good evening or whatever time of day you find yourself in.  I've just finished the first tag in a git repository I've put together to track UNIX developments as documented in the manual.  In preparation for this, I also created restorations of the V1 and V2 manuals in roff based on the available V3 sources.  The repositories for all this can be found here:

https://gitlab.com/segaloco/mandiff
https://gitlab.com/segaloco/v1man
https://gitlab.com/segaloco/v2man

There are most certainly typos and minor discrepancies still to be found between sources and the PDF scans, but given all the cross-referencing involved I believe the manual restorations to be largely complete.

As for the mandiff repository, the commit log (which might shake up in format...) should capture relatively complete transactions of either a particular feature or comparable additions, deletions, or modifications.  That said, there may be little fixes in later commits of edits that really should've been in other ones, and the toc and index accuracy at any given commit is dubious at best.  However, the content of the pages themselves should be pretty well broken up in to noteworthy transactions.

If you find a problem or have a correction, feel free to send it my way or even better, open a pull request with an explanation for the change.  This repository will accrete more UNIX releases as time goes on, with a first goal being getting to V6, after which the fragmentary pathways will be a little harder to reconcile to a single informative trunk.  I might start branches at that point.

By the way, in this process I found that the V2 manual scanned by Dennis Ritchie [1] contains references to nroff(I) in the TOC and permuted index, but the page may not have been in his copy.  Given this, just to not hang up on it, I simply dropped in the V3 page with a note about this in the BUGS section.

1 - https://www.tuhs.org/Archive/Distributions/Research/Dennis_v2/v2man.pdf 

- Matt G.

From marc.donner at gmail.com  Tue Apr  4 03:55:02 2023
From: marc.donner at gmail.com (Marc Donner)
Date: Mon, 3 Apr 2023 13:55:02 -0400
Subject: [TUHS] Warning: April Fools
In-Reply-To: <2f3bf40b-af75-f899-58a1-611b8c52a8c7@gmail.com>
References: <em1be8dba3-c3b7-4aaa-be88-f6ab071b6e45@ad99ed46.com>
 <CAD2gp_TjDavZwXPuzOnaBodd5jYiUQe608AQ2Ho56mzhQPLrew@mail.gmail.com>
 <CAGfO01y15ECtvirSnTgKba9C201KkXuPYjCF6mf=ypFy67hSvA@mail.gmail.com>
 <2148ce0c-e49d-8789-d632-1c3ec27698e8@gmail.com>
 <2f3bf40b-af75-f899-58a1-611b8c52a8c7@gmail.com>
Message-ID: <CALQ0xCC2utDY4iaiwKS-K70RwTNo87h3WA=m05bOKbWnb_2hew@mail.gmail.com>

And for you history nerds, there's an interesting new life of Charlemagne
out from the UC Press (
https://www.ucpress.edu/book/9780520383210/king-and-emperor).  It's a heavy
slog but fascinating.
=====
nygeek.net
mindthegapdialogs.com/home <https://www.mindthegapdialogs.com/home>


On Sun, Apr 2, 2023 at 9:46 AM Harald Arnesen <skogtun at gmail.com> wrote:

> Wesley Parish [02/04/2023 13.18]:
>
> > It's referring to King Chuck the Great, Karl der Gross, Charlemagne. Or
> > if you like, the Great and Magnificent King Upchuck. :) )
>
> Karlamagnus, as the vikings called him.
> --
> Hilsen Harald
> Слава Україні!
>
>
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://www.tuhs.org/pipermail/tuhs/attachments/20230403/95222def/attachment.htm>

From royce at techsolvency.com  Wed Apr  5 14:35:18 2023
From: royce at techsolvency.com (Royce Williams)
Date: Tue, 4 Apr 2023 20:35:18 -0800
Subject: [TUHS] administrivia: redirects from minnie?
Message-ID: <CA+E3k92oyMr_BLS7Pi4cv5E8bHrrJ2YxhZ0KGdcZTBoyrHJ_fA@mail.gmail.com>

I see that all the old minnie links:

https://minnie.tuhs.org/pipermail/*

... break, and archived post are now at:

https://www.tuhs.org/pipermail/*

Could we get a courtesy redirect for pipermail/* ?

-- 
Royce
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://www.tuhs.org/pipermail/tuhs/attachments/20230404/c6756073/attachment.htm>

From henry.r.bent at gmail.com  Thu Apr  6 06:35:06 2023
From: henry.r.bent at gmail.com (Henry Bent)
Date: Wed, 5 Apr 2023 16:35:06 -0400
Subject: [TUHS] 2.9BSD PUCC
Message-ID: <CAEdTPBe1+tzJe2D3v=BNm7r-QJBgiS5tyAV4PtmXKcmsETrCVQ@mail.gmail.com>

Hello all,

I am working on restoring the 2.9BSD PUCC distribution as provided on the
CSRG CD/DVD set, hopefully as closely as possible to the original system.
I am at a point where I have a setup that boots 95% of the way to multiuser
but I have encountered some difficulties with the final few steps.  I would
appreciate it if anyone who is familiar with the login procedure could
contact me off-list.

-Henry
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://www.tuhs.org/pipermail/tuhs/attachments/20230405/e5c03e9a/attachment.htm>

From crossd at gmail.com  Fri Apr  7 06:49:52 2023
From: crossd at gmail.com (Dan Cross)
Date: Thu, 6 Apr 2023 16:49:52 -0400
Subject: [TUHS] Warren Toomey interviewed by Rik Farrow.
Message-ID: <CAEoi9W4r2fsERcEfASuO3FeXjLbdxQ56TP4MC=xtwzSnhFfymg@mail.gmail.com>

Enjoy this interview with our own wkt!

https://www.usenix.org/publications/loginonline/interview-warren-toomey-founder-unix-heritage-society

From lars at nocrew.org  Sat Apr  8 04:00:13 2023
From: lars at nocrew.org (Lars Brinkhoff)
Date: Fri, 07 Apr 2023 18:00:13 +0000
Subject: [TUHS] CWRU Univac artifacs
Message-ID: <7wo7nzshci.fsf@junk.nocrew.org>

Hello,

I received word from someone who went to Case Wester Reserve
Univsersity, and is willing to send early 1970s ephemera to someone
interested in going through it.  The description is:

"I've go stuff from my course work done on our Univac 1108/ChiOS system,
program listing, cpu code cards, etc."

Any takers?

Best regards,
Lars Brinkhoff

From cowan at ccil.org  Sat Apr  8 07:35:11 2023
From: cowan at ccil.org (John Cowan)
Date: Fri, 7 Apr 2023 17:35:11 -0400
Subject: [TUHS] CWRU Univac artifacs
In-Reply-To: <7wo7nzshci.fsf@junk.nocrew.org>
References: <7wo7nzshci.fsf@junk.nocrew.org>
Message-ID: <CAD2gp_SKFnzy9HeUnEZwjjtPJLUL=3kPE5n0YYQWG3Nz_b4dSQ@mail.gmail.com>

I attended CRWU in 1975-76 and programmed the 1108 (abs, alphabetic,
arccos, arcsin, arctan) with punch cards so I am definitely interested if
the material is still available.



On Fri, Apr 7, 2023 at 2:00 PM Lars Brinkhoff <lars at nocrew.org> wrote:

> Hello,
>
> I received word from someone who went to Case Wester Reserve
> Univsersity, and is willing to send early 1970s ephemera to someone
> interested in going through it.  The description is:
>
> "I've go stuff from my course work done on our Univac 1108/ChiOS system,
> program listing, cpu code cards, etc."
>
> Any takers?
>
> Best regards,
> Lars Brinkhoff
>
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://www.tuhs.org/pipermail/tuhs/attachments/20230407/6216c903/attachment.htm>

From lars at nocrew.org  Sat Apr  8 17:16:02 2023
From: lars at nocrew.org (Lars Brinkhoff)
Date: Sat, 08 Apr 2023 07:16:02 +0000
Subject: [TUHS] CWRU Univac artifacs
In-Reply-To: <CAD2gp_SKFnzy9HeUnEZwjjtPJLUL=3kPE5n0YYQWG3Nz_b4dSQ@mail.gmail.com>
 (John Cowan's message of "Fri, 7 Apr 2023 17:35:11 -0400")
References: <7wo7nzshci.fsf@junk.nocrew.org>
 <CAD2gp_SKFnzy9HeUnEZwjjtPJLUL=3kPE5n0YYQWG3Nz_b4dSQ@mail.gmail.com>
Message-ID: <7w7cumsv2l.fsf@junk.nocrew.org>

Apologies, this was meant to go to another mailing list.  I also posted
to COFF, so send any follow-ups there.

John Cowan wrote:
> I attended CRWU in 1975-76 and programmed the 1108 (abs, alphabetic, arccos,
> arcsin, arctan) with punch cards so I am definitely interested if the
> material is still available.

Thank you, I'll fill you in on the details.

