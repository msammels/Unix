dwb_1.0.tar.gz
--------------
From: ozan yigit

   I have a complete distro of dwb. was obtained by me while I was at the
   Dept. of Computer Science at York University to drive our Imagen laser
   printer. The content is simply man pages and sources.

   Nothing else was included in the tape. (no long copyright notice etc).
   Whatever paperwork we had filled to get the tape probably lost in the
   paper clippings of time.

   The files indicate this is "DOCUMENTER'S WORKBENCH 1.0". The file dates
   are from 1986, but I don't recall when we actually requested the tape.
