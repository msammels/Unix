/* Include files for the various programs
 * Author: Warren Toomey wkt@cs.adfa.edu.au
 * $Revision: 1.2 $
 */

#include <sys/types.h>
#include <utime.h>
#include <stdio.h>
#include <string.h>

#include <stdlib.h>                             /* malloc's definition */
#include <time.h>                               /* ctime's definition */
#include <unistd.h>                             /* chown's definition */
#include <sys/stat.h>                           /* chmod's definition */

/* Typedefs */
#ifdef NEEDITYPES
typedef char int8_t;                            /* These may need redefining */
typedef short int16_t;
typedef int int32_t;
#endif

#ifdef NEEDUTYPES
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
#endif
