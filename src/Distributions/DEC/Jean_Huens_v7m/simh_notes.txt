			Installing V7M on SimH
			-----------------------
			Warren Toomey, Apr 2017

Here is as far as I got with SimH 4.0.

Creating the Install Tape
-------------------------

mktape tapefile1 tapefile2 tapefile3  tapefile4  tapefile5 \
        -b 10240 tapefile6 -b 10240 tapefile7.tar -b 10240 tapefile8.tar \
        > v7m.tap


Installing V7M onto the RP06
----------------------------

Here is the install.ini SimH config file:

  set cpu 11/44 1M
  set cpu idle
  attach tm0 v7m.tap
  set rp0 rp06
  attach rp0 rp06.dsk
  set dz enabled
  set dz lines=8
  set dz 8b
  attach -am dz 4000
  boot tm0
  #boot rp0

I followed the instructions in setup.txt. When you boot SimH's pdp11
the first time, answer Y to owerwrite the last track on the rp06.dsk.
Below is a copy of the install sequence with the commands I entered.

Boot
: tm(0,3)
file sys size: 9600
interleave factor: 11
blocks per cylinder: 418
file system: hp(0,0)
isize = 3072
m/n = 11 418
Exit called
Boot
: tm(0,4)
Tape? tm(0,5)
Disk? hp(0,0)
Last chance before scribbling on disk. 
End of tape
Boot
: hp(0,0)hptmunix


unix/v7m 2.1

mem = 999424
# date 8108231300
Sun Aug 23 13:00:00 EDT 1981
# cd /dev
# make rp06
rm -f rp* >/dev/null
rm -f rrp* >/dev/null
rm -f swap >/dev/null
/etc/mknod rp0 b 6 0
/etc/mknod swap b 6 1
/etc/mknod rp2 b 6 2
/etc/mknod rp3 b 6 4
/etc/mknod rrp0 c 14 0
/etc/mknod rrp2 c 14 2
/etc/mknod rrp3 c 14 4
chmod go-w rp0 swap rp2 rp3 rrp0 rrp2 rrp3
# make tm
rm -f mt* >/dev/null
rm -f rmt* >/dev/null
rm -f nrmt* >/dev/null
/etc/mknod mt0 b 3 0
/etc/mknod rmt0 c 12 0
/etc/mknod nrmt0 c 12 128
chmod go+w mt0 rmt0 nrmt0
# dd if=/mdec/hpuboot of=/dev/rp0 count=1
0+1 records in
0+1 records out
# /etc/mkfs /dev/rrp2 08778
isize = 2808
m/n = 3 500
# /etc/mount /dev/rp2 /sys
# cd /sys
# dd if=/dev/nrmt0 of=/dev/null bs=20b files=6
314+103 records in
314+98 records out
# tar40 xbf 20 /dev/rmt0
# cd /
# sync
# /etc/umount /dev/rp2
# /etc/mkfs /dev/rrp3
313082
isize = 65496
m/n = 3 500
# /etc/mount /dev/rp3 /usr
# cd /usr
# dd if=/dev/nrmt0 of=/dev/null bs=20b files=7
547+104 records in
547+98 records out
# tar40 xbf 20 /dev/rmt0
# cd /
# sync
# /etc/umount /dev/rp3
# /etc/mount /dev/rp2 /sys
# /etc/mount /dev/rp3 /usr
# <Enter ctrl-D>

# Restricted rights: Use, duplication, or disclosure
is subject to restrictions stated in your contract with
Western Electric Company, Inc.
Sun Aug 23 13:04:55 EDT 1981

login: root

Welcome to unix/v7m 2.1

# sync
# sync
# sync
Simulation stopped, PC: 003726 (MOV (SP)+,177776)
sim> q
Goodbye

Booting from the RP06
---------------------

Change install.ini to have this at the bottom:

  #boot tm0
  boot rp0

Then run pdp11 install.ini:

PDP-11 simulator V4.0-0 Beta
...
Modem control activated
Auto disconnect activated

#hptmunix


unix/v7m 2.1

mem = 999424
# < type ctrl-D >
# Restricted rights: Use, duplication, or disclosure
is subject to restrictions stated in your contract with
Western Electric Company, Inc.
Sun Aug 23 13:05:36 EDT 1981

login: root

Welcome to unix/v7m 2.1

# 
